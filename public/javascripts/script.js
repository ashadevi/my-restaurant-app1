$(document).ready(function () {
  $("#getMessage").on("click", function () {

    select();
  });
  //--------------------------------------------------SEARCH BY CITY-----------------------------------------

  function select() {
    var valueDropdown = $('#select_id').val();
    var searchCity = "&q=" + valueDropdown;
    var settings = {

      "url": "https://developers.zomato.com/api/v2.1/search?entity_id=" + valueDropdown + "&entity_type=city" + searchCity + "&count=100",
      "method": "GET",
      "headers": {
        "user-key": "862a927a607a674251086b14267deadc",
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }

    $.getJSON(settings, function (data) {

      data = data.restaurants;
      var html = "";

      $.each(data, function (index, value) {
        var x = data[index];
        $.each(x, function (index, value) {
          var location = x.restaurant.location;
          var userRating = x.restaurant.user_rating;
          var button = "booksuccess";
          html += "<div style='width:50%;margin-left:400px;'>"
          html += "<div class='data img-rounded'>";
          html += "<div class='rating'>";
          html += "<span title='" + userRating.rating_text + "'><p style='color:white;background-color:#" + userRating.rating_color + ";border-radius:4px;border:none;padding:2px 10px 2px 10px;text-align: center;text-decoration:none;display:inline-block;font-size:16px;float:right;'><strong>" + userRating.aggregate_rating + "</strong></p></span><br>";
          html += "  <strong class='text-info'>" + userRating.votes + " votes</strong>";
          html += "</div>";
          html += "<img class='resimg img-rounded' src=" + value.thumb + " alt='Restaurant Image' height='185' width='185'>";
          html += "<h2 style='color:red;'><strong>" + value.name + "</strong></h2>";
          html += "  <strong class='text-primary'>" + location.locality + "</strong><br>";
          html += "  <h6 style='color:grey;'><strong>" + location.address + "</strong></h6>";
          html += "  <strong>CUISINES</strong>: " + value.cuisines + "<br>";
          html += "  <strong>COST FOR TWO</strong>: " + value.currency + value.average_cost_for_two + "<br>";
          html += "</hr>";
          html += "  <div><a href=" + button + "><button>Book Table</button></a><br></div>";
          html += "</div><br>";
          html += "</div>"
        });
      });
      $(".message").html(html);
    });

  }
  //--------------------------------------------------------------------------------------------------------

});

var myIndex = 0;
carousel(); //automatic slide show
function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > x.length) {
    myIndex = 1
  }
  x[myIndex - 1].style.display = "block";
  setTimeout(carousel, 2000); // Change image every 2 seconds
}