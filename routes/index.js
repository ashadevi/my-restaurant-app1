
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index');
});
/* GET about page. */
router.get('/about', function (req, res, next) {
  res.render('about');
});
/* GET booksuccess psge */
router.get('/booksuccess', function (req, res, next) {
  res.render('booksuccess');
});
/* GET contact page. */
router.get('/contact', function (req, res, next) {
  res.render('contact');
});
/* GET book page. */
router.get('/book', function (req, res, next) {
  res.render('book');
});

module.exports = router;